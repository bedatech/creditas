
class Ebook < Item
  # use type to distinguish each kind of product: physical, book, digital, membership, etc.
  def initialize(name:)
    super
    @types = ['ebook', 'digital']
  end
end