class Item
  # use type to distinguish each kind of product: physical, book, digital, membership, etc.
  attr_reader :name, :types

  def initialize(name)
    @name = name
  end
  
  def type_of?(type, types)
    types.include?(type)
  end
end