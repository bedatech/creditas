# Desafio para Backend Software Engineer
>Foram criadas classes que eram produtos e classe de entrega dos produtos e seus métodos 

## Arquivos
>Foram separadas cada classe em seu respectivo arquivo

## class Item
>Super classe `Item` para outras sub-classes Book, Ebook, Media, Membership

### type_of? type
> método `type_o?` que verifica qual o tipo do item

## class Deliver
>Classe `Deliver` recebe um pagamento e um produto é encarregada da entrega dos itens conforme seu tipo

#### delivery
>método `delivery` responsável para averiguar qual é a entrega conforme o tipo do item

#### send_email
>método `send_email` responsável pelo envio de email ao cliente

## class Book
>classe `Book` que é do tipo book e phisical e é isenta de taxas

### shipping_label
>método `shipping_label` que printa a label do endereço de entrega e se é isenta de taxas


## class Membership
>classe `Membership` de assinaturas

### activated
>método `activated` que verifica se o item esta ativo

##  class Ebook
>classe `Ebook` de livro digital

## class Media
>classe para representar produtos digitais como musica e video


## class Customer
>classe `Customer` inseridos atributos de email e nome

## class Payment
>classe `Payment` foi alterada para quantificar o desconto para cada item do tipo media digital com a variavel `voucher`