
class Membership < Item
  # you can customize this class by yourself
  attr_accessor :active
  
  def initialize(name, active = false)
    super(name)
    @active = active
    @types = ['signature', 'digital']
  end
  
  def activated?
    active
  end
  
end