
class Book < Item
  # use type to distinguish each kind of product: physical, book, digital, membership, etc.
  def initialize(name)
    super
    @types = ['book', 'phisical']
  end
  
  def shipping_label(address)
    "#{address}"
    'FREE TAX' if type_of?('book', @types)
  end
  
end