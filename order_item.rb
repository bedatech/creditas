class OrderItem
  attr_reader :order, :item

  def initialize(order:, item:)
    @order = order
    @item = item
  end

  def voucher
    item.type_of?('media', item.types) ? 10 : 0
  end
  
  def total
    10
  end
  
end