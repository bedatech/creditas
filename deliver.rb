class Deliver
  attr_reader :payment, :item
  def initialize(payment, item)
    @payment, @item = payment, item
  end
  
  def delivery
    if payment.paid?
      if item.type_of?('signature', item.types)
        self.send_email
        item.active = true
      elsif item.type_of?('media', item.types)
        self.send_email
        "#{item.name}"
      elsif item.type_of?('phisical', item.types)
        item.shipping_label(payment.order.address)
      elsif item.type_of?('book', item.types)
        item.shipping_label(payment.order.address)
      end
    end
  end
  
  def send_email
    payment.order.customer.email
  end

end