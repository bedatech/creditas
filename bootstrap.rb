require_relative 'item'
require_relative 'customer'
require_relative 'deliver'
require_relative 'book'
require_relative 'media'
require_relative 'ebook'
require_relative 'membership'
require_relative 'order'
require_relative 'credit_card'
require_relative 'payment'


# Book Example (build new payments if you need to properly test it)
me = Customer.new('Marcio', 'marcio@mail.com')

# media = Media.new('Awesome Media')
book = Book.new('Awesome book')
# membership = Membership.new('Awesome Membership')

book_order = Order.new(me)
book_order.add_item(book)
# book_order.add_item(book)
# book_order.add_item(membership)


payment = Payment.new(order: book_order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
payment.pay
deliver = Deliver.new(payment, book)
p "Deliver: #{deliver.delivery}"
p "Paid?: #{payment.paid?}" # < true
p "Discount: #{payment.voucher}%"
# now, how to deal with shipping rules then?