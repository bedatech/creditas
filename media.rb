
class Media < Item
  # use type to distinguish each kind of item: physical, book, digital, membership, etc.
  def initialize(name)
    super
    @types = ['media', 'digital']
  end
  
end